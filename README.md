# Get Data Aliases
--------
***GET:*** /account/**:accountId**/aliases?type=**:aliasType**&name=**:aliasTypeName**  
***URL Example:*** https://api-wasteexpert.cloudsustainability.com/account/A4EA0E02-34D1-4FA0-AB02-A915BF86613B/aliases?type=site&name=site101  
***Requires Authentication:*** Yes  
The Get Data Aliases endpoint allows users to retrieve a list of aliases for a given account, alias data type and alias item name.  
Endpoint:	
--------
#### URL Parameters

| Name  | Description | Type | Required |
| :----: | :----: | :----: | :----: |
|  accountId | The organisation public token  | String  | Yes |
|  aliasType | The type of the alias  | String  | Yes |
|  aliasedItemName | The alias item name  | String  | Yes |

#### Header Data Parameters

| Name | Description | Expected Value | Required |
| :----: | :----: | :----: | :----: |
| Authorization | This will be the authToken received during the Authenticate call  | String | Yes |

#### Response Codes
| Code | Outcome |
| :----: | :----: |
| 200 | Success |
| 400 | Invalid accountId, type or name |
| 401 | Client cannot be authenticated |
| 401 | Invalid user token |
| 403 | Client does not have access to the specified accountId |
| 500 | Internal Server Error |

#### Response Data
|Response Name | Description |Example |
| :----: | :----: | :---- |
| type | Aliased data type | "type":"site" |
| name | Name of an aliased item | "name": "Building 101" |
| aliases | Array of aliases | "aliases": [...]

<p style="color: red;">Hello World</p>

#### Full Sample Request
```html
GET https://api-wasteexpert.cloudsustainability.com/account/A4EA0E02-34D1-4FA0-AB02-A915BF86613B/aliases?type=site&name=B101
```

#### Sample Successful Response
```json
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
{
	"aliases": [
		{  }
	]
}
```
#### Notes

N/A

------------------